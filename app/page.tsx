import { Metadata } from 'next';
import { Registro } from '@/components/formularios';

export const metadata: Metadata = {
  title: 'Formulario de registro',
  description:
    'Formulario de registro para la escuela de docentes Univalle, sede Sucre.',
};

export default function Home() {
  return (
    <main className="container mx-auto p-6">
      <Registro />
    </main>
  );
}
