# Laboratorio Unidad 1 y 2

Proyecto de Next.js para la materia de Programacion Web II, formulario para registrar docentes de la universidad Univalle Sede Sucre.

## Ejecutar el proyecto

Ejecutar el servidor de desarrollo:

```bash
npm run dev
```

Abre [http://localhost:3000](http://localhost:3000) para ver el resultado.
