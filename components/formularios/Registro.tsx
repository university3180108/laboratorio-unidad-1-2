'use client';
import { useState } from 'react';
import { format } from 'date-fns';
import { es } from 'date-fns/locale';
import { zodResolver } from '@hookform/resolvers/zod';
import { useForm } from 'react-hook-form';
import { z } from 'zod';
import { cn } from '@/lib/utils';
import {
  Button,
  Calendar,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  Input,
  Form,
  FormField,
  FormItem,
  FormLabel,
  FormControl,
  FormMessage,
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
  RadioGroup,
  RadioGroupItem,
  Popover,
  PopoverContent,
  PopoverTrigger,
} from '@/components/ui';

import { CalendarIcon } from '@radix-ui/react-icons';

const years: string[] = [];
for (let i = new Date().getFullYear(); i >= 1900; i--) {
  years.push(i.toString());
}

const departamentos = [
  'La Paz',
  'Oruro',
  'Potosi',
  'Cochabamba',
  'Chuquisaca',
  'Tarija',
  'Pando',
  'Beni',
  'Santa Cruz',
] as const;

const formSchema = z.object({
  nombres: z.string().min(2, {
    message: 'El nombre es obligatorio',
  }),
  primerApellido: z.string().min(1, {
    message: 'El apellido es obligatorio',
  }),
  segundoApellido: z.string().min(1, {
    message: 'El apellido es obligatorio',
  }),
  departamento: z.enum(departamentos, {
    errorMap: () => ({ message: 'Selecciona un departamento' }),
  }),
  direccion: z.string().min(1, {
    message: 'La direccion es obligatoria',
  }),
  celular: z.string().refine((celular) => !isNaN(parseFloat(celular))),
  email: z.string().email({ message: 'Escribe un correo valido' }),
  sexo: z.string().min(1, {
    message: 'Selecciona una opcion',
  }),
  ci: z.string().refine((ci) => !isNaN(parseFloat(ci))),
  complemento: z.string().min(1, {
    message: 'Obligatorio',
  }),
  fechaNacimiento: z.date({
    errorMap: () => ({ message: 'Escoje una fecha' }),
  }),
});

export const Registro = () => {
  const [open, setOpen] = useState(false);

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      nombres: '',
      primerApellido: '',
      segundoApellido: '',
      departamento: 'Chuquisaca',
      direccion: '',
      celular: '',
      email: '',
      sexo: '',
      ci: '',
      complemento: '',
    },
  });

  function onSubmit(values: z.infer<typeof formSchema>) {
    console.log(values);
    setOpen(true);
  }

  return (
    <>
      <Form {...form}>
        <form
          onSubmit={form.handleSubmit(onSubmit)}
          className="space-y-4 max-w-screen-lg mx-auto"
        >
          <h1 className="text-center text-xl sm:text-2xl">
            Formulario de reigstro docente
          </h1>
          <FormField
            control={form.control}
            name="nombres"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Nombres</FormLabel>
                <FormControl>
                  <Input
                    {...field}
                    className={`${
                      form.formState.errors.nombres &&
                      'border-red-500 focus-visible:ring-red-500'
                    }`}
                  />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />
          <div className="grid md:grid-cols-2 gap-4">
            <FormField
              control={form.control}
              name="primerApellido"
              render={({ field }) => (
                <FormItem>
                  <FormLabel>Primer apellido</FormLabel>
                  <FormControl>
                    <Input
                      {...field}
                      className={`${
                        form.formState.errors.primerApellido &&
                        'border-red-500 focus-visible:ring-red-500'
                      }`}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="segundoApellido"
              render={({ field }) => (
                <FormItem>
                  <FormLabel>Segundo apellido</FormLabel>
                  <FormControl>
                    <Input
                      {...field}
                      className={`${
                        form.formState.errors.segundoApellido &&
                        'border-red-500 focus-visible:ring-red-500'
                      }`}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>
          <div className="grid md:grid-cols-2 gap-4">
            <FormField
              control={form.control}
              name="departamento"
              render={({ field }) => (
                <FormItem>
                  <FormLabel>Departamento</FormLabel>
                  <Select
                    onValueChange={field.onChange}
                    defaultValue={field.value}
                  >
                    <FormControl>
                      <SelectTrigger>
                        <SelectValue placeholder="Selecciona un departamento" />
                      </SelectTrigger>
                    </FormControl>
                    <SelectContent>
                      {departamentos.map((dep) => (
                        <SelectItem key={dep} value={dep}>
                          {dep}
                        </SelectItem>
                      ))}
                    </SelectContent>
                  </Select>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="direccion"
              render={({ field }) => (
                <FormItem>
                  <FormLabel>Direccion</FormLabel>
                  <FormControl>
                    <Input
                      {...field}
                      className={`${
                        form.formState.errors.direccion &&
                        'border-red-500 focus-visible:ring-red-500'
                      }`}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>
          <div className="grid md:grid-cols-2 gap-4">
            <FormField
              control={form.control}
              name="email"
              render={({ field }) => (
                <FormItem>
                  <FormLabel>Correo</FormLabel>
                  <FormControl>
                    <Input
                      {...field}
                      className={`${
                        form.formState.errors.email &&
                        'border-red-500 focus-visible:ring-red-500'
                      }`}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="celular"
              render={({ field }) => (
                <FormItem>
                  <FormLabel>Celular</FormLabel>
                  <FormControl>
                    <Input
                      {...field}
                      className={`${
                        form.formState.errors.celular &&
                        'border-red-500 focus-visible:ring-red-500'
                      }`}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>
          <div className="grid md:grid-cols-2 gap-4">
            <div className="flex gap-4">
              <FormField
                control={form.control}
                name="ci"
                render={({ field }) => (
                  <FormItem className="grow">
                    <FormLabel>CI</FormLabel>
                    <FormControl>
                      <Input
                        {...field}
                        className={`${
                          form.formState.errors.celular &&
                          'border-red-500 focus-visible:ring-red-500'
                        }`}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="complemento"
                render={({ field }) => (
                  <FormItem className="w-28 grow-0">
                    <FormLabel>Complemento</FormLabel>
                    <FormControl>
                      <Input
                        {...field}
                        className={`${
                          form.formState.errors.complemento &&
                          'border-red-500 focus-visible:ring-red-500'
                        }`}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </div>
            <FormField
              control={form.control}
              name="sexo"
              render={({ field }) => (
                <FormItem className="space-y-5">
                  <FormLabel>Sexo</FormLabel>
                  <FormControl>
                    <RadioGroup
                      onValueChange={field.onChange}
                      defaultValue={field.value}
                      className="flex gap-8"
                    >
                      <FormItem className="flex items-center space-x-3 space-y-0">
                        <FormControl>
                          <RadioGroupItem value="M" />
                        </FormControl>
                        <FormLabel className="font-normal">Masculino</FormLabel>
                      </FormItem>
                      <FormItem className="flex items-center space-x-3 space-y-0">
                        <FormControl>
                          <RadioGroupItem value="F" />
                        </FormControl>
                        <FormLabel className="font-normal">Femenino</FormLabel>
                      </FormItem>
                    </RadioGroup>
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>
          <div className="grid md:grid-cols-2 gap-4">
            <div className="flex gap-4 mt-4 sm:mt-0">
              <FormField
                control={form.control}
                name="fechaNacimiento"
                render={({ field }) => (
                  <FormItem className="flex flex-col grow">
                    <FormLabel>Año nacimiento</FormLabel>
                    <Select
                      onValueChange={(value) => {
                        field.onChange(new Date(parseInt(value), 0, 1));
                      }}
                      defaultValue=""
                      value={field.value?.getFullYear().toString() ?? ''}
                    >
                      <FormControl>
                        <SelectTrigger
                          className={`${
                            form.formState.errors.fechaNacimiento &&
                            'border-red-500 focus-visible:ring-red-500'
                          }`}
                        >
                          <SelectValue placeholder="Selecciona un departamento" />
                        </SelectTrigger>
                      </FormControl>
                      <SelectContent>
                        {years.map((year) => (
                          <SelectItem key={year} value={year}>
                            {year}
                          </SelectItem>
                        ))}
                      </SelectContent>
                    </Select>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="fechaNacimiento"
                render={({ field }) => (
                  <FormItem className="flex flex-col grow-0">
                    <FormLabel>Fecha de nacimiento</FormLabel>
                    <Popover>
                      <PopoverTrigger asChild>
                        <FormControl>
                          <Button
                            variant={'outline'}
                            className={cn(
                              'w-[220px] pl-3 text-left font-normal',
                              !field.value && 'text-muted-foreground',
                              `${
                                form.formState.errors.fechaNacimiento &&
                                'border-red-500 focus-visible:ring-red-500'
                              }`
                            )}
                          >
                            {field.value ? (
                              format(field.value, 'PPP', {
                                locale: es,
                              })
                            ) : (
                              <span>Escoje una fecha</span>
                            )}
                            <CalendarIcon className="ml-auto h-4 w-4 opacity-50" />
                          </Button>
                        </FormControl>
                      </PopoverTrigger>
                      <PopoverContent className="w-auto p-0" align="start">
                        <Calendar
                          mode="single"
                          defaultMonth={field.value}
                          selected={field.value}
                          onSelect={field.onChange}
                          disabled={(date) =>
                            date > new Date() || date < new Date('1900-01-01')
                          }
                          initialFocus
                        />
                      </PopoverContent>
                    </Popover>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </div>
          </div>
          <div className="flex flex-col gap-4 sm:flex-row sm:justify-center pt-6">
            <Button type="submit" className="w-full sm:w-fit">
              Guardar
            </Button>
            <Button type="button" variant="outline" className="w-full sm:w-fit">
              Cancelar
            </Button>
          </div>

          <Dialog open={open} onOpenChange={(value) => setOpen(value)}>
            <DialogContent className="sm:max-w-[525px]">
              <DialogHeader>
                <DialogTitle>Información</DialogTitle>
                <DialogDescription>
                  Todos los datos del formulario
                </DialogDescription>
              </DialogHeader>
              <div className="grid gap-4 py-4 overflow-x-auto">
                <pre>{JSON.stringify(form.getValues(), null, 2)}</pre>
              </div>
              <DialogFooter>
                <Button type="button" onClick={() => setOpen(false)}>
                  OK
                </Button>
              </DialogFooter>
            </DialogContent>
          </Dialog>
        </form>
      </Form>
    </>
  );
};
